package com.devcamp.j5920.j5920;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J5920Application {

	public static void main(String[] args) {
		SpringApplication.run(J5920Application.class, args);
	}

}
