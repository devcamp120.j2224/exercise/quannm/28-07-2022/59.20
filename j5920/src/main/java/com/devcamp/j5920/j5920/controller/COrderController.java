package com.devcamp.j5920.j5920.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j5920.j5920.model.COrder;
import com.devcamp.j5920.j5920.reposiroty.IOrderReposiroty;

@CrossOrigin
@RestController
@RequestMapping("/")
public class COrderController {
    @Autowired
    IOrderReposiroty pIOrderReposiroty;
    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllOrder() {
        try {
            List<COrder> lOrders = new ArrayList<>();
            pIOrderReposiroty.findAll().forEach(lOrders::add);
            return new ResponseEntity<List<COrder>>(lOrders, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
